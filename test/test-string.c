/* Copyright © 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of Fenrir Operating System.
 *
 * Fenrir is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fenrir is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Fenrir.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#define TEST_STRING "123"
#define TEST_STRING_MEMSET "111"

#define ARRAY_LEN(x) (sizeof (x) / sizeof (x[0]))

int
main (void)
{
    const char string[] = TEST_STRING;
    char copy[ARRAY_LEN (string)];
    bool pass = true;

    /* memcpy */
    pass &= (memcpy (copy, string, strlen (string) + 1) == copy);
    /* memmove */
    pass &= (memmove (copy, copy, strlen (copy)) == copy);
    /* memcmp */
    pass &= (memcmp (string, copy, strlen (string)) == 0);
    /* strcmp */
    pass &= (strcmp (copy, copy) == 0);
    /* strncmp */
    pass &= (strncmp (copy, copy, strlen (copy)) == 0);
    /* memchr */
    pass &= (memchr (string, '2', 2) == &string[1]);
    /* strchr */
    pass &= (strchr (string, '3') == &string[2]);
    /* memset */
    pass &= (memset (copy, '1', ARRAY_LEN (copy)) == copy);
    pass &= (strcmp (copy, TEST_STRING_MEMSET) == 0);
    /* strlen */
    pass &= (strlen (string) == 3);

    return pass? EXIT_SUCCESS : EXIT_FAILURE;
}
