/* Copyright © 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of Fenrir Operating System.
 *
 * Fenrir is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fenrir is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Fenrir.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <string.h>

#include <stdbool.h>

void *
memcpy (void       *restrict s1,
        const void *restrict s2,
        size_t               n)
{
    if (s1 == NULL || s2 == NULL || s1 == s2)
    {
        return s1;
    }

    while (n-- > 0)
    {
        unsigned char *restrict c1;
        const unsigned char *restrict c2;

        c1 = s1;
        c2 = s2;

        *(c1++) = *(c2++);
    }

    return s1;
}

void *
memmove (void       *s1,
         const void *s2,
         size_t      n)
{
    bool reverse;
    unsigned char *c1;
    const unsigned char *c2;
    int direction;

    if (s1 == NULL || s2 == NULL || s1 == s2)
    {
        return s1;
    }

    /* Are the two regions potentially overlapping? */
    reverse = s1 > s2;

    c1 = s1;
    c2 = s2;

    /* Fast-forward if the regions are potentially overlapping. */
    c1 += reverse * (n - 1);
    c2 += reverse * (n - 1);

    direction = 1 - (2 * reverse);

    while (n-- > 0)
    {
        *c1 = *c2;

        c1 += direction;
        c2 += direction;
    }

    return s1;
}


int
memcmp (const void *s1,
        const void *s2,
        size_t      n)
{
    const unsigned char *c1;
    const unsigned char *c2;

    if (s1 == s2)
    {
        return 0;
    }

    c1 = s1;
    c2 = s2;

    while (n-- > 0)
    {
        if (*c1 > *c2)
        {
            return 1;
        }
        else if (*c1 < *c2)
        {
            return -1;
        }

        c1++;
        c2++;
    }

    return 0;
}

int
strcmp (const char *s1,
        const char *s2)
{
    if (s1 == s2)
    {
        return 0;
    }

    while (*s1 != 0 && *s2 != 0)
    {
        if (*s1 > *s2)
        {
            return 1;
        }
        else if (*s1 < *s2)
        {
            return -1;
        }

        s1++;
        s2++;
    }

    return 0;
}

int
strncmp (const char *s1,
         const char *s2,
         size_t      n)
{
    if (s1 == s2)
    {
        return 0;
    }

    while (n-- && *s1 != 0 && *s2 != 0)
    {
        if (*s1 > *s2)
        {
            return 1;
        }
        else if (*s1 < *s2)
        {
            return -1;
        }

        s1++;
        s2++;
    }

    return 0;
}


void *
memchr (const void *s,
        int         c,
        size_t      n)
{
    const unsigned char *bytes;

    if (s == NULL)
    {
        return NULL;
    }

    bytes = s;

    while (n--)
    {
        if (*bytes == c)
        {
            return (void *) bytes;
        }

        bytes++;
    }

    return NULL;
}

char *
strchr (const char *s,
        int         c)
{
    if (s == NULL)
    {
        return NULL;
    }

    while (*s != 0)
    {
        if (*s == c)
        {
            return (char *) s;
        }

        s++;
    }

    return NULL;
}


void *
memset (void  *s,
        int    c,
        size_t n)
{
    unsigned char *cs;

    if (s == NULL)
    {
        return NULL;
    }

    cs = s;

    while (n-- > 0)
    {
        *(cs++) = c;
    }

    return s;
}

size_t
strlen (const char *s)
{
    size_t len = 0;

    while (*s != 0)
    {
        len++;
        s++;
    }

    return len;
}

size_t
strnlen (const char *s,
         size_t      maxlen)
{
    size_t len = 0;

    while (maxlen-- > 0 && *(s++) != 0)
    {
        len++;
    }

    return len;
}
