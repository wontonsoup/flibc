/* Copyright © 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of Fenrir Operating System.
 *
 * Fenrir is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fenrir is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Fenrir.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <stddef.h>

void *memcpy  (void       *restrict dest,
               const void *restrict src,
               size_t               n);
void *memmove (void       *dest,
               const void *src,
               size_t      n);

int memcmp  (const void *lhs,
             const void *rhs,
             size_t      n);
int strcmp  (const char *lhs,
             const char *rhs);
int strncmp (const char *lhs,
             const char *rhs,
             size_t      n);

void *memchr (const void *haystack,
              int         needle,
              size_t      n);
char *strchr (const char *haystack,
              int         needle);

void   *memset (void       *dest,
                int         c,
                size_t      n);
size_t  strlen (const char *string);

size_t strnlen (const char *string,
                size_t      n);
