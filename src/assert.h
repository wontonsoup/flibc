/* Copyright © 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of Fenrir Operating System.
 *
 * Fenrir is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fenrir is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Fenrir.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#ifndef NDEBUG
void __assert (const char *expression,
               const char *function,
               const char *file,
               size_t      line);

#define assert(expression) (__assert (#expression, __func__, __FILE__, __LINE__))
#else
#define assert(ignore) ((void) 0)
#endif
