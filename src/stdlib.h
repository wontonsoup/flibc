#pragma once

#define EXIT_FAILURE 1
#define EXIT_SUCCESS 0

unsigned long int strtoul (const char *restrict, char **restrict, int);
