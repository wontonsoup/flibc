/* Copyright © 2017 Ernestas Kulik <ernestas DOT kulik AT gmail DOT com>
 *
 * This file is part of Fenrir Operating System.
 *
 * Fenrir is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Fenrir is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Fenrir.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdlib.h>

#include <ctype.h>
#include <stddef.h>

static unsigned long int
strtoul_10 (const char *restrict nptr,
            char **restrict      endptr)
{
    int i = -1;
    int converted = 0;

    if (nptr == NULL)
    {
        return 0;
    }

    while (*nptr != 0)
    {
        if (i == -1 && isspace (*nptr))
        {
            nptr++;

            continue;
        }

        i++;

        if (i == 0 && *nptr == '+')
        {
            nptr++;

            continue;
        }

        if (!(*nptr >= '0' && *nptr <= '9'))
        {
            if (endptr != NULL)
            {
                *endptr = (char *restrict)nptr;
            }

            break;
        }

        converted *= 10;
        converted += *nptr - '0';

        nptr++;
    }

    return converted;
}

unsigned long int
strtoul (const char *restrict nptr,
         char **restrict      endptr,
         int                  base)
{
    switch (base)
    {
        case 10:
        {
            return strtoul_10 (nptr, endptr);
        }
        break;
    }

    return 0;
}
